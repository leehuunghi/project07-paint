1. Lê Hữu Nghị - 1512349 - leehuunghi@gmail.com
2. Những gì đã làm:
	+ xây dựng chương trình vẽ 5 loại hình cơ bản.
	+ Xây dựng menu chọn hình cần vẽ có CHECKED
	+ Giữ phím shift để chuyển chữ nhật sang vuông, ellipse sang tròn
	+ Bọc tất cả các đối tượng vẽ vào các lớp model.
Sử dụng đa xạ (polymorphism) để cài đặt việc quản lý các đối tượng và vẽ hình. Sử dụng mẫu thiết kế prototypes để tạo ra hàng mẫu nhằm vẽ ở chế độ xem trước (preview),
 sử dụng mẫu thiết kế Factory để tạo mới đối tượng
3. Luồng sự kiện chính: Người dùng mở chương trình chọn hình cần vẽ và kéo chuột với kích thước tùy ý.
4. Luồng sự kiện phụ:
	+ Người dùng chọn hình chữ nhật sau đó nhấn shift để thành hình vuông rồi thả shift để trở lại hình chữ nhật. Tương tự cho ellipse và tròn
5. Link bitbucket: https://bitbucket.org/leehuunghi/project07-paint
6. Link youtube: https://youtu.be/Y41jK9YjF6I