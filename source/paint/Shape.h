#pragma once
#include <utility>
#include <windowsX.h>
#include <string>
#include <math.h>		
using namespace std;

class CShape
{
protected:
	pair<int, int> m_Src, m_Des;
public:
	CShape();
	virtual void draw(HDC hdc)=0;
	virtual CShape* create()=0;
	void setData(pair<int, int> pSrc, pair<int, int> pDes){ m_Src = pSrc; m_Des = pDes; };
	static string className();
	~CShape();
};

