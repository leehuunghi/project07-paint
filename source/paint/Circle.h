#pragma once
#include "Shape.h"
class CCircle :
	public CShape
{
public:
	CCircle();
	void draw(HDC hdc);
	void setData(pair<int, int> pSrc, pair<int, int> pDes);
	CShape* create(){ return new CCircle; };
	static string className(){ return "circle"; };
	~CCircle();
};

