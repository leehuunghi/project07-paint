#include "stdafx.h"
#include "Square.h"


CSquare::CSquare()
{

}

void CSquare::draw(HDC hdc)
{
	int edge = abs(m_Des.first - m_Src.first) < abs(m_Des.second - m_Src.second) ? abs(m_Des.first - m_Src.first) : abs(m_Des.second - m_Src.second);

	if (m_Des.second > m_Src.second && m_Des.first < m_Src.first)
	{
		m_Des.first = m_Src.first - edge;
		m_Des.second = m_Src.second + edge;
	}
	else if (m_Des.second<m_Src.second&&m_Des.first>m_Src.first)
	{
		m_Des.first = m_Src.first + edge;
		m_Des.second = m_Src.second - edge;
	}
	else if (m_Des.second > m_Src.second&&m_Des.first > m_Src.first)
	{
		m_Des.first = m_Src.first + edge;
		m_Des.second = m_Src.second + edge;
	}
	else
	{
		m_Des.first = m_Src.first - edge;
		m_Des.second = m_Src.second - edge;
	}
	Rectangle(hdc, m_Src.first, m_Src.second, m_Des.first, m_Des.second);
}

CSquare::~CSquare()
{
}
