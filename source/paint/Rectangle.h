#pragma once
#include "Shape.h"
class CRectangle :
	public CShape
{
public:
	CRectangle();
	void draw(HDC hdc);
	void setData(pair<int, int> pSrc, pair<int, int> pDes);
	CShape* create(){ return new CRectangle; };
	static string className(){ return "rectangle"; };
	~CRectangle();
};

