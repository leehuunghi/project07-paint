#pragma once
#include "Shape.h"
class CSquare :
	public CShape
{
public:
	CSquare();
	void draw(HDC hdc);
	void setData(pair<int, int> pSrc, pair<int, int> pDes);
	CShape* create(){ return new CSquare; };
	static string className(){ return "square"; };
	~CSquare();
};

