#pragma once
#include "Circle.h"
#include "Line.h"
#include "Rectangle.h"
#include "Square.h"
#include "Ellipse.h"

class CShapeFactory
{
private:
	CShapeFactory();
public:
	static CShape* Get(string name);
	~CShapeFactory();
};

