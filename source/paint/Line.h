#pragma once
#include "Shape.h"
class CLine :
	public CShape
{
public:
	CLine();
	void draw(HDC hdc);
	void setData(pair<int, int> pSrc, pair<int, int> pDes);
	CShape* create(){ return new CLine; };
	static string className(){ return "line"; };
	~CLine();
};