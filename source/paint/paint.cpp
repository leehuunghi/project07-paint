// paint.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "paint.h"
#include <windowsx.h>
#include "ShapeFactory.h"
#include <vector>

//enum TYPESHAPE{LINE,RECTANGLE,SQUARE,ELLIPSE,CIRCLE,NOSHAPE};

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_PAINT, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_PAINT));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_PAINT));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_PAINT);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

vector<CShape*> shapes;
vector<CShape*> prototypes;

int typeShape;
bool isDrawing = FALSE;
pair<int, int> pointStart, pointEnd, pointTemp;
CShape* shapeTemp;
//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
HBRUSH hBrush2;
HPEN hPen;
int flagIDMenu = IDC_LINE;
bool ctrlShift;
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	HMENU hMenu = GetMenu(hWnd);
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc,hdcTemp;
	switch (message)
	{
	case WM_CREATE:
		prototypes.push_back(CShapeFactory::Get("line"));
		prototypes.push_back(CShapeFactory::Get("rectangle"));
		prototypes.push_back(CShapeFactory::Get("square"));
		prototypes.push_back(CShapeFactory::Get("ellipse"));
		prototypes.push_back(CShapeFactory::Get("circle"));
		CheckMenuItem(hMenu, flagIDMenu, MF_CHECKED);

		break;

	case WM_LBUTTONDOWN:
		pointStart.first = GET_X_LPARAM(lParam);
		pointStart.second = GET_Y_LPARAM(lParam);

		if (!isDrawing)
		{
			isDrawing = true;
			pointTemp = pointStart;
			shapeTemp = prototypes[typeShape]->create();
		}

		break;
	case WM_MOUSEMOVE:
		if (isDrawing)
		{
			
			pointTemp.first = GET_X_LPARAM(lParam);
			pointTemp.second = GET_Y_LPARAM(lParam);
			InvalidateRect(hWnd, NULL, TRUE);
		}
		break;
	case WM_LBUTTONUP:
		pointEnd.first = GET_X_LPARAM(lParam);
		pointEnd.second = GET_Y_LPARAM(lParam);
		shapeTemp->setData(pointStart, pointEnd);
		shapes.push_back(shapeTemp);
		isDrawing = false;
		InvalidateRect(hWnd, NULL, TRUE);

		break;
	case WM_KEYDOWN: 
	{
		if (wParam == VK_SHIFT) {
			if (isDrawing) delete shapeTemp;
			if (flagIDMenu == IDC_RECTANGLE) typeShape = 2;
			if (flagIDMenu == IDC_ELLIPSE) typeShape = 4;
			shapeTemp = prototypes[typeShape]->create();
		}
		break;
	}
	case WM_KEYUP:
	{
		if (wParam == VK_SHIFT) {
			if (isDrawing) delete shapeTemp;
			if (flagIDMenu == IDC_RECTANGLE&&typeShape == 2) typeShape = 1;
			if (flagIDMenu == IDC_ELLIPSE&&typeShape == 4) typeShape = 3;
			shapeTemp = prototypes[typeShape]->create();
		}
		break;
	}
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		
		// Parse the menu selections:
		switch (wmId)
		{
		case IDC_LINE:
			CheckMenuItem(hMenu, flagIDMenu, MF_UNCHECKED);
			flagIDMenu = IDC_LINE;
			CheckMenuItem(hMenu, flagIDMenu, MF_CHECKED);
			typeShape = 0;
			break;
		case IDC_RECTANGLE:
			CheckMenuItem(hMenu, flagIDMenu, MF_UNCHECKED);
			flagIDMenu = IDC_RECTANGLE;
			CheckMenuItem(hMenu, flagIDMenu, MF_CHECKED);			
			typeShape = 1;
			break;
		case IDC_SQUARE:
			CheckMenuItem(hMenu, flagIDMenu, MF_UNCHECKED);
			flagIDMenu = IDC_SQUARE;
			CheckMenuItem(hMenu, flagIDMenu, MF_CHECKED);			
			typeShape = 2;
			break;
		case IDC_ELLIPSE:
			CheckMenuItem(hMenu, flagIDMenu, MF_UNCHECKED);
			flagIDMenu = IDC_ELLIPSE;
			CheckMenuItem(hMenu, flagIDMenu, MF_CHECKED);			
			typeShape = 3;
			break;
		case IDC_CIRCLE:
			CheckMenuItem(hMenu, flagIDMenu, MF_UNCHECKED);
			flagIDMenu = IDC_CIRCLE;
			CheckMenuItem(hMenu, flagIDMenu, MF_CHECKED);			
			typeShape = 4;
			break;
		case IDM_ABOUT1:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT1:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:
		PAINTSTRUCT ps;
		hdc = BeginPaint(hWnd, &ps);

		hPen = CreatePen(PS_SOLID, 1, RGB(255, 11, 11));
		SelectObject(hdc, hPen);

		hBrush2 = CreateSolidBrush(RGB(234, 234, 7));
		SelectObject(hdc, hBrush2);

		// TODO: Add any drawing code here...
		for (int i = 0; i < shapes.size(); i++)
		{
			shapes[i]->draw(hdc);
		}

		if (isDrawing) {
			shapeTemp->setData(pointStart, pointTemp);
			shapeTemp->draw(hdc);

		}
		
		DeleteObject(hBrush2);
		DeleteObject(hPen);
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
